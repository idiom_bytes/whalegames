### Whale Games Nextjs app

- Sign up for ethplorer @ ethplorer.io or use the free key that's the fallback
- Make sure you have `npm` and `node` setup
- Run `npm install`
- Setup a PG instance and create the tables using the SQL in `db/sql/tables.sql`
- Create a `.env` file in the root directory with these keys:
```
ETHPLORER_API_KEY
DB_HOST
DB_PORT
DB_USERNAME
DB_PASSWORD
DB_DATABASE
VERCEL_URL=http://localhost:3000
```
- Run `npm run dev` and visit `localhost:3000`