import React from "react";
import axios from 'axios';
import Head from 'next/head'
import _ from 'lodash';
import api from './api';
import Layout, { siteTitle } from '../components/layout'
import RatioChart from '../components/ratiochart';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Link } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import moment from 'moment';


const useStyles = makeStyles({
  table: {
    minWidth: 850,
  },
  root: {
    flexGrow: 1,
    backgroundColor: 'grey',
  },
});


function numberWithCommas(x) {
  return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
}

function average(nums) {
  return nums.reduce((a, b) => a + b) / nums.length;
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function Home(props) {
  const classes = useStyles();

  const xamp = props.xamp;
  const tob = props.tob;

  const xampHolders = xamp.holders.map((holder) => holder.address);
  const tobHolders = tob.holders.map((holder) => holder.address);
  const EXCLUDED_WALLETS = [
    "0x0000000000000000000000000000000000000001",
    "0x37ee8c0695b3dd657a3640d62f2ae0a8bd8fe3cf",
    "0xf911a7ec46a2c6fa49193212fe4a2a9b95851c27",
    "0x28bc0c76a5f8f8461be181c0cbddf715bc1d96af",
    // 0xe41e5FA65d197AFA059edCe5225C1da2a01A361C // dev
  ];
  const holdBoth = xampHolders.filter((holder) => {
    return _.includes(tobHolders, holder) && !_.includes(EXCLUDED_WALLETS, holder);
  });
  const unsortedXampOnly =
    xamp.holders.filter((holder) => {
      return (
        !_.includes(tobHolders, holder.address) &&
        !_.includes(EXCLUDED_WALLETS, holder.address)
      );
    });
  const xampOnly = _.reverse(_.sortBy(unsortedXampOnly, ["balance"]));

  const unsortedTobOnly = tob.holders.filter((holder) => {
    return (
      !_.includes(xampHolders, holder.address) &&
      !_.includes(EXCLUDED_WALLETS, holder.address)
    );
  });

  const tobOnly = _.reverse(_.sortBy(unsortedTobOnly, ['balance']));
  const unsortedBothHolderRatios = holdBoth.map((address) => {
    const xampWallet = _.find(xamp.holders, {address});
    const tobWallet = _.find(tob.holders, {address});
    const xampValueInEth =
      xamp.currentPrice.ETH * (xampWallet.balance);
    const tobValueInEth = tob.currentPrice.ETH * tobWallet.balance;
    const totalEthValue = xampValueInEth + tobValueInEth;
    return {
      address,
      xampValueInEth: Math.ceil(xampValueInEth * 100) / 100,
      xampBalance: Math.ceil(xampWallet.balance * 100) / 100,
      tobBalance: Math.ceil(tobWallet.balance * 100) / 100,
      tobValueInEth: Math.ceil(tobValueInEth * 100) / 100,
      totalEthValue: Math.ceil(totalEthValue * 100) / 100,
      ratio: (xampValueInEth / totalEthValue) * 100,
    };
  });

  const bothHolderRatios = _.reverse(_.sortBy(unsortedBothHolderRatios, [
    "totalEthValue",
  ]));
  console.log(bothHolderRatios);

  const ratioArray = bothHolderRatios.map(({ratio}) => ratio);
  const totalXampInEthArray = bothHolderRatios.map(
    ({ xampValueInEth }) => xampValueInEth
  );
  const tobValueInEthArray = bothHolderRatios.map(
    ({ tobValueInEth }) => tobValueInEth
  );

  const totalXampInEth = totalXampInEthArray.reduce((a, b) => a + b, 0);
  const tobValueInEth = tobValueInEthArray.reduce((a, b) => a + b, 0);
  console.log(totalXampInEth);
  console.log("totalXampInEth: ", totalXampInEth);
  console.log("tobValueInEth: ", tobValueInEth);
  const combinedTotalValue = Math.ceil((totalXampInEth + tobValueInEth) * 100) / 100;
  const xampRatioInTotalValue =
    (Math.ceil((totalXampInEth / combinedTotalValue) * 100) / 100) * 100;
  const xampRatioAverage = average(ratioArray);
  const isXampLarger = xampRatioAverage > 50;
   const [value, setValue] = React.useState(0);

   const handleChange = (event, newValue) => {
     setValue(newValue);
   };
  return (
    <Layout>
      <Container maxWidth={false}>
        <Head>
          <title>{siteTitle}</title>
          <script
            async
            defer
            data-domain="whalegames.co"
            src="https://plausible.io/js/plausible.js"
          ></script>
        </Head>
        <div>
          <center>
            <h3>
              Current ratio in ETH:{" "}
              {tob.currentPrice.ETH / xamp.currentPrice.ETH}
            </h3>
          </center>
          <center>
            If you find this tool useful, please donate to mi familia: {"  "}
            <Link
              href="https://etherscan.io/address/0x3E597Ec4a398e0a75b288CeE9Fb65f9405bd7141"
              target="_blank"
              rel="noopener"
            >
              0x3E597Ec4a398e0a75b288CeE9Fb65f9405bd7141
            </Link>
            <br />
            <p>Last updated {moment(props.lastDeploy).fromNow()}</p>
          </center>
          <AppBar position="static">
            <Tabs value={value} onChange={handleChange} aria-label="tabs">
              <Tab label="Hold both XAMP + TOB" {...a11yProps(0)} />
              <Tab label="XAMP Only" {...a11yProps(1)} />
              <Tab label="TOB Only" {...a11yProps(2)} />
            </Tabs>
          </AppBar>
          <TabPanel value={value} index={1}>
            <TableContainer component={Paper}>
              <Table className={classes.table} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>
                      <b>Rank</b>
                    </TableCell>
                    <TableCell>
                      <b>Address</b>
                    </TableCell>
                    <TableCell align="right">
                      <b>XAMP Balance</b>
                    </TableCell>
                    <TableCell align="right">
                      <b>XMAP Share</b>
                    </TableCell>
                    <TableCell align="right">
                      <b>ETH Value</b>
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {xampOnly.map((row, index) => (
                    <TableRow key={row.address}>
                      <TableCell component="th" scope="row">
                        {index + 1}
                      </TableCell>
                      <TableCell align="right">
                        <Link
                          href={`https://etherscan.io/address/${row.address}`}
                          target="_blank"
                          rel="noopener"
                        >
                          {row.address}
                        </Link>
                      </TableCell>
                      <TableCell align="right">
                        {numberWithCommas(Math.ceil(row.balance * 100) / 100)}
                      </TableCell>
                      <TableCell align="right">{row.share}%</TableCell>
                      <TableCell align="right">
                        {Math.ceil(xamp.currentPrice.ETH * row.balance * 100) /
                          100}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </TabPanel>
          <TabPanel value={value} index={2}>
            <TableContainer component={Paper}>
              <Table className={classes.table} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>
                      <b>Rank</b>
                    </TableCell>
                    <TableCell>
                      <b>Address</b>
                    </TableCell>
                    <TableCell align="right">
                      <b>TOB Balance</b>
                    </TableCell>
                    <TableCell align="right">
                      <b>TOB Share</b>
                    </TableCell>
                    <TableCell align="right">
                      <b>ETH Value</b>
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {tobOnly.map((row, index) => (
                    <TableRow key={row.address}>
                      <TableCell component="th" scope="row">
                        {index + 1}
                      </TableCell>
                      <TableCell align="right">
                        <Link
                          href={`https://etherscan.io/address/${row.address}`}
                          target="_blank"
                          rel="noopener"
                        >
                          {row.address}
                        </Link>
                      </TableCell>
                      <TableCell align="right">
                        {numberWithCommas(Math.ceil(row.balance * 100) / 100)}
                      </TableCell>
                      <TableCell align="right">{row.share}%</TableCell>
                      <TableCell align="right">
                        {Math.ceil(tob.currentPrice.ETH * row.balance * 100) /
                          100}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </TabPanel>
          <TabPanel value={value} index={0}>
            <div>
              <br />
              <center>
                Average of top wallets XAMP/TOB:
                <RatioChart
                  xampRatio={average(ratioArray)}
                  tobRatio={100 - average(ratioArray)}
                />
                <h2>
                  Average ratio of{" "}
                  <u>{Math.ceil(average(ratioArray) * 100) / 100}%</u> with{" "}
                  {isXampLarger
                    ? "XAMP being larger than TOB"
                    : "TOB being larger than XAMP"}
                </h2>
                <h3>
                  {isXampLarger
                    ? "XAMP winning the ratio war"
                    : "TOB winning the ratio war"}
                </h3>
                <h3>
                  Top wallets own a combined {combinedTotalValue} ETH of XAMP +
                  TOB
                </h3>
                <h3>
                  XAMP's ratio of total combined ETH value is{" "}
                  {xampRatioInTotalValue}%
                </h3>
                <br />
                {/* TODO add sorting */}
              </center>
              <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell>
                        <b>Rank</b>
                      </TableCell>
                      <TableCell>
                        <b>Address</b>
                      </TableCell>
                      <TableCell align="right">
                        <b>XAMP Balance</b>
                      </TableCell>
                      <TableCell align="right">
                        <b>XAMP ETH Value</b>
                      </TableCell>
                      <TableCell align="right">
                        <b>TOB Balance</b>
                      </TableCell>
                      <TableCell align="right">
                        <b>TOB ETH Value</b>
                      </TableCell>
                      <TableCell align="right">
                        <b>Total Value in ETH</b>
                      </TableCell>
                      <TableCell align="right">
                        <b>% of Total Combined Value</b>
                      </TableCell>
                      <TableCell align="right">
                        <b>XAMP % for XAMP/TOB Pair</b>
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {bothHolderRatios.map((row, index) => (
                      <TableRow key={row.address}>
                        <TableCell component="th" scope="row">
                          {index + 1}
                        </TableCell>
                        <TableCell align="right">
                          <Link
                            href={`https://etherscan.io/address/${row.address}`}
                            target="_blank"
                            rel="noopener"
                          >
                            {row.address}
                          </Link>
                        </TableCell>
                        <TableCell align="right">
                          {numberWithCommas(row.xampBalance)}
                        </TableCell>
                        <TableCell align="right">
                          {row.xampValueInEth}
                        </TableCell>
                        <TableCell align="right">
                          {numberWithCommas(row.tobBalance)}
                        </TableCell>
                        <TableCell align="right">{row.tobValueInEth}</TableCell>
                        <TableCell align="right">{row.totalEthValue}</TableCell>
                        <TableCell align="right">
                          {(Math.ceil(
                            (row.totalEthValue / combinedTotalValue) * 100
                          ) /
                            100) *
                            100}
                          %
                        </TableCell>
                        <TableCell align="right">
                          {Math.ceil(row.ratio * 100) / 100}%
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
          </TabPanel>
        </div>
        <Link href="https://ethplorer.io/" target="_blank" rel="noopener">
          Powered by Ethplorer.io
        </Link>
        <br />
        <Link href="https://www.coingecko.com/" target="_blank" rel="noopener">
          Powered by CoinGecko API
        </Link>
      </Container>
    </Layout>
  );
}

export const getStaticProps = async (ctx) => {
  // TODO have client side request latest prices / ratios
  console.log(process.env.VERCEL_URL);
  let data;
  if (!!process.env.VERCEL_URL && !_.includes(process.env.VERCEL_URL, 'localhost')) {
    data = await api.getData();
  } else {
    const res = await axios.get(`${process.env.VERCEL_URL}/api/sync`);
    data = res.data;
  }
  return {
    props: {
      xamp: data.xamp,
      tob: data.tob,
      lastDeploy: new Date().toUTCString(),
    },
  };
};

export default Home;