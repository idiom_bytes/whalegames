import {getData} from './sync';
import {getCoingeckoData} from './getCoingeckoData';
import {getEthplorData} from './getEthdata';
export default {
  getData,
  getCoingeckoData,
  getEthplorData,
};