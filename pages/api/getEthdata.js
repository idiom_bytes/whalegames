import axios from "axios";
import _ from "lodash";
import dotenv from "dotenv";
import * as db from "../../db";
dotenv.config();

const getBalanceForToken = (token, balance) => {
  switch (token.slug) {
    case "antiample":
      return balance / 1000000000;
    case "tokens-of-babel":
      const num = Number(balance).toLocaleString("fullwide", {
        useGrouping: false,
      });
      return _.toNumber(num) / 1000000000000000000;
    default:
      return null;
  }
};

const getData = async (token) => {
  const hasRequestedRecently = await db.hasRequestedRecently({
    provider: "ethplorer",
    type: token.slug,
  });
  if (hasRequestedRecently) {
    // TODO is right? lol
    const wallets = await db.selectWallets();
    const balances = await db.selectActiveBalances(token.id);
    return {
      token,
      wallets,
      balances,
    };
  }

  const API_KEY = process.env.ETHPLORER_API_KEY || "freekey";
  const LIMIT = 1000;
  const URL = `https://api.ethplorer.io/getTopTokenHolders/${token.address}?apiKey=${API_KEY}&limit=${LIMIT}`;

  const { data } = await axios.get(URL);
  await db.createRequest({ provider: "ethplorer", type: token.id });

  const walletIds = data.holders.map((holder) => ({ address: holder.address }));
  let existingWallets = await db.selectWallets();
  const existingWalletIds = existingWallets.map(({ address }) => address);
  const newWalletIds = walletIds.filter((walletId) => {
    return !_.includes(existingWalletIds, walletId.address);
  });

  const wallets = await db.createWallets(newWalletIds);

  const existingBalances = await db.selectActiveBalances(token.id);
  if (existingBalances && existingBalances.length) {
    // Update balances
    await db.setBalancesToStale(token.id);
  }

  const balances = data.holders.map((holder) => {
    const wallet = _.find(wallets, { address: holder.address });
    return {
      token_id: token.id,
      wallet_id: wallet.id,
      is_stale: false,
      amount: getBalanceForToken(token, holder.balance),
      share: holder.share,
    };
  });

  const insertedBalances = await db.createBalances(balances, token.id);
  return {
    token,
    wallets,
    balances: insertedBalances,
  };
};

export const getEthplorData = async () => {
  const tokens = await db.selectTokens();
  console.log("runn getData");
  const ethplorData = await Promise.all(tokens.map(getData));
  return ethplorData;
};

export default async (req, res) => {
  try {
    console.log("runn ethplorData");
    const ethplorData = await getEthplorData();

    https: res.status(200).json(ethplorData);
  } catch (error) {
    console.log("Error: ", error);
    https: res.status(200).json(error);
  }
};
