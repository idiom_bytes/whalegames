import _ from 'lodash';
import dotenv from 'dotenv';
import * as db from "../../db";
import requests from './';
dotenv.config();


const mapWalletsToBalances = (ethplor) => {
  const wallets = _.map(ethplor.wallets, (wallet) => {
      const address = wallet.address;
      const balance = _.find(ethplor.balances, (balance) => {
        return balance.wallet_id === wallet.id;
      });
      return {
        address,
        balance: balance ? balance.amount : undefined,
        share: balance ? balance.share : undefined,
      };
  });

  // TODO i fucked up somewhere.. figure it out
  return _.filter(
    wallets,
    (wallet) =>
      wallet.address &&
      !_.isUndefined(wallet.balance) &&
      !_.isUndefined(wallet.share)
  );
}

// TODO XAMP->TOB LP TOKEN TODO add this into calc
// etherscan.io/token/0x28bc0c76a5f8f8461be181c0cbddf715bc1d96af#balances

// TODO see if whales positions have expanded or contracted in the last few days
// TODO expand it to any wallet.
// TODO track their portfolios..
// TODO historical ratio
// TODO create a ratios table to map pair
// TODO track p&l for ETH in?
// TODO *** include decimals so you can dynamically do the maths
// TODO track balance of a token over time for a wallet
export const getData = async () => {
  try {
    const tokens = await db.selectTokens();

    console.log('running requests ethplorData...');
    const ethplorData = await requests.getEthplorData();

    console.log('running requests coingeckoData...');
    const coingeckoData = await requests.getCoingeckoData();

    const xampCoinGecko = _.find(coingeckoData, (priceData) => {
      const xamp = _.find(tokens, {symbol: 'XAMP'});
      return priceData.token_id === xamp.id;
    });

    const xampEthplor = _.find(ethplorData, (ethplor) => {
      const xamp = _.find(tokens, { symbol: "XAMP" });
      return ethplor.token.id === xamp.id;
    });

    const tobCoinGecko = _.find(coingeckoData, (priceData) => {
      const tob = _.find(tokens, { symbol: "TOB" });
      return priceData.token_id === tob.id;
    });

    const tobEthplor = _.find(ethplorData, (ethplor) => {
      const tob = _.find(tokens, { symbol: "TOB" });
      return ethplor.token.id === tob.id;
    });

    // console.log("xampCoinGecko: ", xampCoinGecko);
    // TODO map to handle dynamic amount tokens
    // return _.map(tokens, token => {
    //   [token.symbol]:
    // });
     return {
       xamp: {
         currentPrice: {
           USD: xampCoinGecko.usd_price,
           ETH: xampCoinGecko.eth_price,
         },
         holders: mapWalletsToBalances(xampEthplor),
       },
       tob: {
         currentPrice: {
           USD: tobCoinGecko.usd_price,
           ETH: tobCoinGecko.eth_price,
         },
         holders: mapWalletsToBalances(tobEthplor),
       },
     };
   } catch (error) {
     console.log("Error: ", error);
     return {};
   }
}


export default async (req, res) => {
  try {
    console.log('running sync getData...');
    const data = await getData();
    https: res.status(200).json(data);
  } catch (error) {
     console.log("Error: ", error);
     https: res.status(200).json(error);
   }
};