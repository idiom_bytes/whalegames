import _ from "lodash";
import CoinGecko from "coingecko-api";
import dotenv from "dotenv";
import * as db from "../../db";
dotenv.config();

const getData = async (token) => {
  const hasRequestedRecently = await db.hasRequestedRecently({
    provider: "coingecko",
  });
  if (hasRequestedRecently) {
    return await db.getRecentPrice(token.id);
  }

  const CG_PARAMS = {
    market_data: true,
    tickers: false,
    community_data: false,
    developer_data: false,
    localization: false,
    sparkline: false,
  };

  const CoinGeckoClient = new CoinGecko();
  const { data } = await CoinGeckoClient.coins.fetch(token.slug, CG_PARAMS);

  await db.createRequest({ provider: "coingecko", type: token.slug });

  return await db.insertNewPrice({
    token_id: token.id,
    usd_price: data.market_data.current_price.usd,
    eth_price: data.market_data.current_price.eth,
  });
};

export const getCoingeckoData = async () => {
  const tokens = await db.selectTokens();

  console.log("runn getData");
  const coingeckoData = await Promise.all(tokens.map(getData));
  return coingeckoData;
}

export default async (req, res) => {
  try {
    console.log("runn getCoingeckoData");
    const coingeckoData = await getCoingeckoData();

    https: res.status(200).json(coingeckoData);
  } catch (error) {
    console.log("Error: ", error);
    https: res.status(200).json(error);
  }
};
