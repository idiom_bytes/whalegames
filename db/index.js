import dotenv from "dotenv";
import knexClient from "knex";
import moment from 'moment';
import _ from 'lodash';
dotenv.config();

const knex = knexClient({
  client: "pg",
  connection: {
    user: process.env.DB_USERNAME,
    host: process.env.DB_HOST,
    database: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    acquireConnectionTimeout: 10000,
  },
});

export const selectTokens = async () => {
  const tokens = await knex("tokens").select("*");
  return tokens.map((token) => ({
    ...token,
    id: _.toNumber(token.id),
  }));
}

export const selectWallets = async () => {
  const wallets = await knex("wallets").select("*");

  return wallets.map((wallet) => ({
    ...wallet,
    id: _.toNumber(wallet.id),
  }));
}

export const selectActiveBalances = async (tokenId) => {
  const balances = await knex("balances").select("*").where({
    token_id: tokenId,
    is_stale: false,
  });

  return balances.map(balance => ({
    ...balance,
    id: _.toNumber(balance.id),
    amount: _.toNumber(balance.amount),
    share: _.toNumber(balance.share),
  }));
}

export const setBalancesToStale = async (tokenId) => {
  await knex("balances")
    .update({ is_stale: true })
    .where({ token_id: tokenId });
};

export const createRequest = async (options) => {
  await knex("requests").insert(options);
}

export const createWallets = async (wallets) => {
  await knex("wallets").insert(wallets);
  return await selectWallets();
}

export const createBalances = async (balances, tokenId) => {
  await knex("balances").insert(balances);
  return await selectActiveBalances(tokenId);
};

export const hasRequestedRecently = async (options) => {
  const [ lastRequest ] = await knex
    .select("*")
    .where(options)
    .orderBy("id", "desc")
    .from("requests")
    .limit(1);
    console.log("lastRequest", lastRequest);
   const lastRequestedAt = lastRequest ? moment(lastRequest.created_at) : moment();
   const minutesAgo = moment().subtract(5, 'minute');
   return lastRequest && lastRequestedAt.isAfter(minutesAgo);
}

export const getRecentPrice = async (tokenId) => {
  const prices = await knex("prices")
    .select("*")
    .where({
      token_id: tokenId,
    })
    .limit(1)
    .orderBy("id", "desc");
  return prices[0];
}

export const insertNewPrice = async (options) => {
  await knex("prices").insert(options);
  return await getRecentPrice(options.token_id);
};
